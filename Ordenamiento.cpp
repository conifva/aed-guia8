#include <iostream>
#include <fstream>
#include "Ordenamiento.h"

Ordenamiento::Ordenamiento(){}

/* imprimir el array ordenado*/
void Ordenamiento::imprimir(int *arreglo, int largo) {
	
	int i;
	for (i = 0; i < largo; ++i) {
		cout << "array["<< i << "]=" << arreglo[i] << " ";
	}
	cout << endl;

}

/* Método Burbuja menor */
void Ordenamiento::burbujaMenor(int *arreglo, int largo) {
	
	clock_t start = clock();
	double tiempoOrndenar;
	int temp;
	int i;
	int j;

	/* Primer for recorre el arreglo de forma ascendente y el segundo de forma
	 descendente. Es decir, el primer ciclo va "de atrás hacia adelante", mien-
	 tras que el segundo va de "adelante hacia atrás" moviendo el elemento menor
	 hacia la izquierda del arreglo */
	for (i = 1; i < largo; ++i) {
		for (j = largo - 1; j >= largo; --j) {

			if (arreglo[j - 1] > arreglo[j]) {
				//si la posicion anterior a arreglo[j] es mayor a esta, se iguala
				// una variable temporal al contenido de arreglo[j-1]
				temp = arreglo[j - 1];
				//info en la posicion j-1 es reemplazada por la de la posicion j
				arreglo[j - 1] = arreglo[j];
				//contenido que estaba en la posicion j-1 se guardan en posic j
				arreglo[j] = temp;
			}
		}
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Burbuja Menor     |" << tiempoOrndenar << " milisegundos" << endl;
}

/* Método Burbuja mayor */
void Ordenamiento::burbujaMayor(int *arreglo, int largo) {
	
	clock_t start = clock();
	double tiempoOrndenar;
	int temp;
	int i;
	int j;

	/* Primer for recorre el arreglo de forma descendente y el segundo de forma 
	 ascendente. Es decir, el primer ciclo va "de adelante hacia atrás", mien-
	 tras que el segundo va de "atrás hacia adelante" moviendo el elemento mayor
	 hacia la derecha del arreglo. */
	/* Inverso de burbuja menor, pero ambos ordenando de menor a mayor */
	for (i = largo - 2; i > 0; --i) {
		for (j = 0; j < i; ++j) {
			
			if (arreglo[j] > arreglo[j + 1]) {
				//si la info en la pisicion j+1 es menos a la posicion en j, se
				//guarda la info contenida en la posicion j en variable temporal
				temp = arreglo[j];
				//se reemplaza la info en la posicion j por la en j+1
				arreglo[j] = arreglo[j + 1];
				//info en temporal se guarda en la posicion j+1
				arreglo[j + 1] = temp;
			}
		}
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Burbuja Mayor     |" << tiempoOrndenar << " milisegundos" << endl;

}

/* Método inserción */
void Ordenamiento::insercion(int* arreglo, int largo) {

	clock_t start = clock();
	double tiempoOrndenar;
	int temp;
	int k;
	int i;

	/* COMO ORDENAR CARTAS
	 Tomo la primera y la coloco en mi mano. Luego tomo la segunda y la comparo
	 con la que tengo: si es mayor, la pongo a la derecha, y si es menor a la
	 izquierda. Después tomo la tercera y la comparo con las que tengo en la ma-
	 no, desplazándola hasta que quede en su posición final. Continúo haciendo
	 esto, insertando cada carta en la posición que le corresponde, hasta que
	 las tengo todas en orden.*/
	
	for (i = 1; i < largo; ++i) {
		//info contenida en la posicion i se guarda en un int temporal
		temp = arreglo[i];
		k = i-1; //k se iguala a i-1

		/* Ciclo while dura mientras k no sea negativo y mientras la info en la
		 posicion k sea mayor a la guardada en temp */
		while (k >= 0 && temp < arreglo[k]) {
			//Dentro del while se toma la info de la posicion k y se guarda en
			// la posicion k+1
			arreglo[k+1] = arreglo[k];
			k--; //k disminuye (a k-1)
		}
		//Al salir del while, la info de temp se guarda en la posicion k+1 
		arreglo[k+1] = temp;
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Insercion         |" << tiempoOrndenar << " milisegundos" << endl;
}

/* Método inserción Binaria */
void Ordenamiento::insercionBinaria(int* arreglo, int largo) {

	clock_t start = clock();
	double tiempoOrndenar;
	int temp;
	int izq; //primer dato
	int der; //ultimo dato
	int m; //dato central
	int i;
	int j;

	/* Es una mejora del método de inserción directa, ya que se hace una búsque-
	 da binaria en lugar de una búsqueda secuencial para insertar el elemento a 
	 la izquierda del arreglo, que ya se encuentra ordenado. Y el proceso se re-
	 pite hasta el n-esimo elemento. */
	for (i = 1; i < largo; ++i) {
		//la informacion contenida en la posicion i se guarda en un int temporal
		temp = arreglo[i];
		izq = 0; //variable izq se iguala a 0 (primero)
		der = i - 1; //der se iguala a i-1 (ultimo)

		/* busqueda binaria de la posicion de insercion */
		while (izq <= der) {
			m = ((izq + der) / 2); //iguala m a la parte entera del promedio entre iz y der

			//si la informacion en temp es menor o igual a la infoen la posicion
			// m
			if (temp <= arreglo[m]) {
				//entonces der se hace m-1 (central -1)
				der = m - 1;
			} else {
				//no se cumple, izq se vuelve m+1 (central +1)
				izq = m + 1;
			}
		}
		//terminado el while, j se iguala a i-1
		j = i - 1;
		
		/* desplazamos a la derecha los elementos ordenados para insertar el nu-
		 evo */
		//se ingresa a un segundo while. Mientras j no sea menor a izq
		while(j >= izq){
			//la info en la posicion j se guarda en la posicion j+1
			arreglo[j+1] = arreglo[j];
			//j se iguala a j-1
			j--;
		}
		//la infoen temp se guarda en la posicion izq del arreglo (primer dato)
		arreglo[izq] = temp;
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Insercion Binaria |" << tiempoOrndenar << " milisegundos" << endl;

}

/* Método de selección */
void Ordenamiento::seleccion(int* arreglo, int largo) {
	clock_t start = clock();
	double tiempoOrndenar;
	int menor;
	int k; //para realizar los intercambios
	int i;
	int j;
	
	/* PASOS: Buscas el elemento más pequeño de la lista.
	 Lo intercambias con el elemento ubicado en la primera posición de la lista.
	 Buscas el segundo elemento más pequeño de la lista.
	 Lo intercambias con el elemento que ocupa la segunda posición en la lista.
	 Repites este proceso hasta que hayas ordenado toda la lista. */

	for (i = 0; i < largo -1 ; ++i) {
		//la info contenida en la posicion i se guarda en un int "menor"
		menor = arreglo[i];
		k = i;
		//Segundo ciclo for iguala j a i+1 y se hace j+1, hasta que j alcanza al
		// tamaño
		for(j = i + 1; j < largo; ++j){
			if(arreglo[j] < menor){
				//reemplaza la info de menor, con la contenida en la posicion j
				menor = arreglo[j];
				k = j; //igualar variables
			}
		}
		//se reemplaza la info en la posicion k por la que esta en la posicion i
		arreglo[k] = arreglo[i];
		//posicion i se llena con la info contenida en menor
		arreglo[i] = menor;
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Seleccion         |" << tiempoOrndenar << " milisegundos" << endl;
}

/* Método de shell */
void Ordenamiento::shell(int* arreglo, int largo) {
	clock_t start = clock();
	double tiempoOrndenar;
	int i;
	int temp;
	int temp1;
	bool band;

	
	while(temp1 > 0){
		temp1 = temp1 / 2;
		band = 1;

		while (band) {
			band = 0;
			i = 0;

			while ((i + temp1) < largo) {
				if (arreglo[i] > arreglo[i + temp1]) {
					temp = arreglo[i];
					arreglo[i] = arreglo[i + temp1];
					arreglo[i + temp1] = temp;
					band = 1;
				}
				i++;
			}
		}
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Shell             |" << tiempoOrndenar << " milisegundos" << endl;
}

/* Inicio Quicksort */
void Ordenamiento::reduceQckSort(int inicio, int fin, int &pos, int *arreglo){

	int temp;
	int izq = inicio;
	int der = fin;
	bool band = 1;

	pos = inicio;

	while (band) {
		while (arreglo[pos] <= arreglo[der] && pos != der) {
			der--;
		}

		if(pos == der) {
			band = 0;
		} else {
			temp = arreglo[pos];
			arreglo[pos] = arreglo[der];
			arreglo[der] = temp;
			pos = der;

			while (arreglo[pos] >= arreglo[izq] && pos != izq) {
				izq++;
				temp = arreglo[pos];
				arreglo[pos] = arreglo[izq];
				arreglo[izq] = temp;
				pos = izq;
			}
		}
	}
}

void Ordenamiento::qckSort(int *arreglo, int largo) {

	clock_t start = clock();
	double tiempoOrndenar;
	int pilaMenor[largo];
	int pilaMayor[largo];
	int inicio;
	int fin;
	int pos;
	int tope = 0;
	pilaMenor[tope] = 0;
	pilaMayor[tope] = largo - 1;

	while(tope >=0 ){
		inicio = pilaMenor[tope];
		fin = pilaMayor[tope];
		tope--;
		this->reduceQckSort(inicio, fin, pos, arreglo);

		if (inicio < (pos - 1)) {
			tope++;
			pilaMenor[tope] = inicio;
			pilaMayor[tope] = pos - 1;
		}

		if(fin > (pos + 1)) {
			tope++;
			pilaMenor[tope] = pos+1;
			pilaMayor[tope] = fin;
		}
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Quicksort         |" << tiempoOrndenar << " milisegundos" << endl;
	cout << " -----------------------------------------" << endl;

}
