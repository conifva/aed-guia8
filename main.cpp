#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Ordenamiento.h"

using namespace std;


void printArreglos(int* arreglo, int* arreglo1, int* arreglo2, int* arreglo3, 
	int* arreglo4, int* arreglo5, int* arreglo6, int* arreglo7, 
	Ordenamiento orden,	int largo) {	

	/* MOSTRAR VECTORES */
	cout << " Original Ordenado |";
	for (int i = 0; i < largo; ++i){
		cout << "array[" << i << "]=" << arreglo[i] << " ";
	}
	cout << endl;
	
	cout << " Burbuja Menor     ";
	orden.imprimir(arreglo1, largo);
	cout << " Burbuja Mayor     ";
	orden.imprimir(arreglo2, largo);
	cout << " Insercion         ";
	orden.imprimir(arreglo3, largo);
	cout << " Insercion Binaria ";
	orden.imprimir(arreglo4, largo);
	cout << " Seleccion         ";
	orden.imprimir(arreglo5, largo);
	cout << " Shell             ";
	orden.imprimir(arreglo6, largo);
	cout << " Quicksort         ";
	orden.imprimir(arreglo7, largo);
	cout << " -----------------------------------------" << endl;
}

void llenar(int* arreglo, int largo, string ver) {
	/* Inicializar objeto que contiene los metodos de ordenamiento interno */
	Ordenamiento orden;
	//Ordenamiento orden = Ordenamiento();

	cout << " Arreglo Original  ";
	/* Llenar arreglo */
	for (int i = 0; i < largo; ++i) {
		/* con números aleatorios  */
		int aleatorio = (rand() % 100000) + 1;
		arreglo[i] = aleatorio;
		cout << "array[" << i << "]=" << arreglo[i] << " ";
	}
	cout << endl;
	
	/* crear arraglos de cada metodo*/
	int *arreglo1 = new int[largo];
	int *arreglo2 = new int[largo];
	int *arreglo3 = new int[largo];
	int *arreglo4 = new int[largo];
	int *arreglo5 = new int[largo];
	int *arreglo6 = new int[largo];
	int *arreglo7 = new int[largo];
	/* asignar los mismos números a todos los arreglos */
	arreglo1 = arreglo;
	arreglo2 = arreglo;
	arreglo3 = arreglo;
	arreglo4 = arreglo;
	arreglo5 = arreglo;
	arreglo6 = arreglo;
	arreglo7 = arreglo;

	//ver == "n"
	/* Tabla de tiempo */
	cout << endl;
	cout << " -----------------------------------------" << endl;
	cout << " Método            |Tiempo" << endl;
	cout << " -----------------------------------------" << endl;
	orden.burbujaMenor(arreglo1, largo);
	orden.burbujaMayor(arreglo2, largo);
	orden.insercion(arreglo3, largo);
	orden.insercionBinaria(arreglo4, largo);
	orden.shell(arreglo5, largo);
	orden.seleccion(arreglo6, largo);
	orden.qckSort(arreglo7, largo);
	
	//ver == "s"
	if (ver == "s") {
		/* Imprimir arraglos ordenados */
		printArreglos(arreglo, arreglo1, arreglo2, arreglo3, arreglo4, arreglo5, 
			arreglo6, arreglo7, orden, largo);
	}
}

int main(int argc, char *argv[]) {

	/* Validar ingreso de los 3 argumentos */
	if (argc != 3) {
		/* no ingreso los 3 argumentos */
		cout << "ERROR. Número argumentos inválido" << endl;
		cout << " Ingrese el largo del arreglo y su visualizacion" << endl;
		cout << " Reinicie el programa" << endl;
		exit(1);

	} else { //ingresó 3 argumentos
		
		/* Comprobar que el segundo sea un entero */
		try{
			int largo = stoi(argv[1]); //segundo argumento es un número
			string ver = argv[2]; //tercer argumento es 

			if (largo == 0) {
				cout << "No hay nada que ordenar" << endl;
				return 0;

			} else if (largo > 1000000 or largo <= 1) {
				/* Verificar rango del largo */
				cout << "ERROR. Número inválido." << endl;
				cout << " Ingrese un valor entre 0 y 1000000" << endl;
				cout << " Reinicie el programa" << endl;
				exit(1);

			} else { //2do parametro es válido
				
				/* Comprobar que el tercero */
				if (ver == "s" or ver == "n"){
					/* Verificar las 2 opciones (s o n)*/
					//tercer argumento es válido
					cout << " Generando arreglo" << endl;//generar array
					
					/*si el parámetro es válido, se crea el arreglo*/
					int *arreglo = new int[largo];
					//string ver = argv[2];
					llenar(arreglo, largo, ver);

				} else { 
					cout << "ERROR. Tercer argumento inválido" << endl;
					cout << " Tercer argumento debe ser de tipo char. Corresponde";
					cout << " al tipo de visualización ('n' o 's')" << endl;
					cout << " Reinicie el programa" << endl;
					exit(1);
				}
			}
		} catch (const std::invalid_argument& e){
			// segundo argumento no es un número
			cout << "ERROR. Segundo argumento inválido" << endl;
			cout << " Segundo argumento debe ser de tipo int con rango";
			cout << "  entre 0 y 1000000" << endl;
			cout << " Reinicie el programa" << endl;
			exit(1);
		}
	}
	return 0;
}