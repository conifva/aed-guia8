#include <iostream>
using namespace std;

#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H

class Ordenamiento {
	public:
		/* Constructor */
		Ordenamiento();
		//~Ordenamiento();

		/* Metodos */
		void burbujaMenor(int *arreglo, int largo);
		void burbujaMayor(int *arreglo, int largo);
		void insercion(int* arreglo, int largo);
		void insercionBinaria(int* arreglo, int largo);
		void seleccion(int* arreglo, int largo);
		void shell(int* arreglo, int largo);
		void qckSort(int* arreglo, int largo);

		void imprimir(int* arreglo, int largo);
	
	private:
		void reduceQckSort(int inicio, int fin, int &pos, int *arreglo);
};
#endif